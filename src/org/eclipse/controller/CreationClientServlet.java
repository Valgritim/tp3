package org.eclipse.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.model.Client;
import org.eclipse.service.AdresseDaoImpl;
import org.eclipse.service.ClientDaoImpl;
import org.eclipse.service.Dao;


/**
 * Servlet implementation class CreationClientServlet
 */
@WebServlet("/creationClient")
public class CreationClientServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE_SUCCESS = "/afficherClient.jsp";
	public static final String VUE_FORM = "/creerClient.jsp";
	private ClientDaoImpl clientDao;
	
	/**
	 * @return 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	
	public void init() {
		clientDao = new ClientDaoImpl();		
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
				
		this.getServletContext().getRequestDispatcher("/creerClient.jsp").forward(request, response);	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("phone");
		String email = request.getParameter("email");
		Client client = new Client(nom,prenom,telephone,email);
		List<String> messages = new ArrayList<String>();
		boolean verifNom = false;
		boolean verifPrenom = false;
		boolean verifEmail = false;
		
		
		try {
			verifNom = verifChaine(nom);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			verifPrenom = verifChaine(prenom);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			verifEmail = verifEmail(email);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (!nom.trim().isEmpty() || !prenom.trim().isEmpty() ||! telephone.isEmpty() || !email.isEmpty()){
			
			if(!verifNom || !verifPrenom || !verifEmail) {
				if(!verifNom) {
					String message = "Votre nom doit contenir au moins deux lettres!\n";
					messages.add(message);
				} 
				if(!verifPrenom) {				
					String message = "Votre prénom doit contenir au moins deux lettres!\n";
					messages.add(message);
				}
				if(!verifEmail) {
					String message = "Caractères invalides dans l'email";					
					messages.add(message);
				}
				request.setAttribute("messages", messages);
				request.setAttribute("client", client);
				this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
			} else {
				
				String message = "Votre client a bien été créé!";
				request.setAttribute("message", message);
				request.setAttribute("client", client);
				clientDao.save(client);
				this.getServletContext().getRequestDispatcher(VUE_SUCCESS).forward(request, response);
			}
		} else {			
			String message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires.";			
			messages.add(message);
			request.setAttribute("messages", message);
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}			
	}
	
	public boolean verifChaine(String s) throws Exception {				
		String regex = "[a-zA-Z+-+\\s]{2,}";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(s);
		boolean b = m.matches();
		return b;
	}
	private boolean verifEmail(String s) {
		String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(s);
		boolean b = m.matches();
		return b;
	}

}

