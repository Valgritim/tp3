package org.eclipse.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.model.Adresse;
import org.eclipse.model.Client;
import org.eclipse.service.AdresseDaoImpl;

/**
 * Servlet implementation class CreationAdresseServlet
 */
@WebServlet("/creationAdresse")
public class CreationAdresseServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE_SUCCESS = "/afficherAdresse.jsp";
	public static final String VUE_FORM = "/creerAdresse.jsp";
	private AdresseDaoImpl adresseDao;
	
	public void init() {
		
		adresseDao = new AdresseDaoImpl();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<String> messages = new ArrayList<String>();
		boolean verifNom = false;
		boolean verifPrenom = false;
		boolean verifRue = false;
		boolean verifVille = false;
		boolean verifCp = false;
		boolean verifLength = false;
		boolean verifEmail = false;
		
		//Creation client
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("phone");
		String email = request.getParameter("email");
		Client client = new Client(nom,prenom,telephone,email);
		
		//creation adresse
		String rue = request.getParameter("rue");			
		String ville = request.getParameter("ville");
		String codePostal = request.getParameter("codePostal");	
		Adresse adresse = new Adresse(client,rue,codePostal,ville);

		
		try {
			verifNom = verifChaine(nom);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			verifPrenom = verifChaine(prenom);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			verifRue = verifChaine(rue);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try {
			verifVille = verifChaine(ville);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();		
		
		}
		
		try {
			verifCp = verifCodePostal(codePostal);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			verifEmail = verifEmail(email);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
    
		if (!nom.trim().isEmpty() || !prenom.trim().isEmpty() ||! telephone.isEmpty() || !email.isEmpty() || !rue.trim().isEmpty() || !ville.trim().isEmpty() || !codePostal.trim().isEmpty()){					
			
			if(!verifNom || !verifPrenom || !verifRue || !verifVille || !verifCp || !verifEmail) {
				if(!verifNom) {
					String message = "Votre nom doit contenir au moins deux lettres!";
					messages.add(message);
				} 
				if(!verifPrenom) {				
					String message = "Votre pr�nom doit contenir au moins deux lettres!";
					messages.add(message);
				}
				if(!verifRue) {				
					String message = "Le nom de la rue doit contenir au moins deux lettres!";
					messages.add(message);
				}
				if(!verifVille) {				
					String message = "Le nom de la ville doit contenir au moins deux lettres!";
					messages.add(message);
				}
				if(!verifCp) {
					String message = "Le code postal doit contenir 5 chiffres!";					
					messages.add(message);
				}
				if(!verifEmail) {
					String message = "Caract�res invalides dans l'email";					
					messages.add(message);
				}
					
				request.setAttribute("nomSaisi", nom);
				request.setAttribute("prenomSaisi", prenom);
				request.setAttribute("phoneSaisi", telephone);
				request.setAttribute("emailSaisi",email);
				request.setAttribute("rueSaisie", rue);			
				request.setAttribute("villeSaisie", ville);
				request.setAttribute("codePostalSaisi", codePostal);	
				request.setAttribute("messages", messages);
				this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
				
			} else {			
				
				String message = "Votre adresse a bien �t� cr��!";
				request.setAttribute("message", message);
				request.setAttribute("adresse", adresse);
				adresseDao.save(client, adresse);
				this.getServletContext().getRequestDispatcher(VUE_SUCCESS).forward(request, response);
			}	

		} else {
			String message = "Erreur - Vous n'avez pas rempli tous les champs obligatoires.";			
			messages.add(message);
			request.setAttribute("messages", message);
			request.setAttribute("nomSaisi", nom);
			request.setAttribute("prenomSaisi", prenom);
			request.setAttribute("phoneSaisi", telephone);
			request.setAttribute("emailSaisi",email);
			request.setAttribute("rueSaisie", rue);			
			request.setAttribute("villeSaisie", ville);
			request.setAttribute("codePostalSaisi", codePostal);
			this.getServletContext().getRequestDispatcher(VUE_FORM).forward(request, response);
		}	
	}
	
		public boolean verifChaine(String s) throws Exception {				

			String regex = "[a-zA-Z]{2,}";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(s);
			boolean b = m.matches();
			return b;
		}
		

		private boolean verifCodePostal (String s){
			String regex = "[\\d]{5}";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(s);
			boolean b = m.matches();
			return b;
		}
		
		private boolean verifEmail(String s) {
			String regex = "^[A-Za-z0-9+_.-]+@(.+)$";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(s);
			boolean b = m.matches();
			return b;
		}

}
