package org.eclipse.service;

import org.eclipse.model.Adresse;

public interface GenericDao <T, Y>{
	
	public void save(T object, Y obj);
}
