package org.eclipse.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.eclipse.model.Client;


public class ClientDaoImpl implements Dao<Client>{

	@Override
	public Client save(Client client) {
		Connection connexion = MyConnection.getConnection();
		
		if(connexion != null) {		
			
				try {
					
					PreparedStatement ps = connexion.prepareStatement("INSERT INTO client (nom,prenom,telephone,email) VALUES (?,?,?,?);");
					
					ps.setString(1, ((Client) client).getNom());
					ps.setString(2, ((Client) client).getPrenom());
					ps.setString(3, ((Client) client).getTelephone());
					ps.setString(4, ((Client) client).getEmail());
					ps.executeUpdate();	
					System.out.println("Le client a �t� cr��e!");
					return client;
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		return null;

	}

}
