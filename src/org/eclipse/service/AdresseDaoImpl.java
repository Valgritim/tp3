package org.eclipse.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.eclipse.model.Adresse;
import org.eclipse.model.Client;

public class AdresseDaoImpl implements GenericDao<Client, Adresse>{

	@Override
	public void save(Client client , Adresse adresse) {
Connection connexion = MyConnection.getConnection();
		
		if(connexion != null) {		
			
				try {
					
					PreparedStatement ps = connexion.prepareStatement("INSERT INTO client (nom,prenom,telephone,email,rue,codePostal,ville) VALUES (?,?,?,?,?,?,?);");
					ps.setString(1, ((Client) client).getNom());
					ps.setString(2, ((Client) client).getPrenom());
					ps.setString(3, ((Client) client).getTelephone());
					ps.setString(4, ((Client) client).getEmail());
					ps.setString(5, ((Adresse) adresse).getRue());
					ps.setString(6, ((Adresse) adresse).getCodePostal());
					ps.setString(7, ((Adresse) adresse).getVille());
					ps.executeUpdate();	
					System.out.println("L'adresse a �t� cr��e!");
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		
	}

}
