package org.eclipse.service;

public interface Dao<T> {

	public T save(T object);
}
