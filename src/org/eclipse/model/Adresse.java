package org.eclipse.model;

public class Adresse {

	private String rue;
	private String codePostal;
	private String ville;
	private Client client;
	
	
	public Adresse(Client client,String rue, String codePostal, String ville ) {
		super();
		this.rue = rue;
		this.codePostal = codePostal;
		this.ville = ville;
		this.client = client;
	}

	public Adresse() {
		super();
	}

	public String getRue() {
		return rue;
	}

	public void setRue(String rue) {
		this.rue = rue;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	@Override
	public String toString() {
		return "Adresse [rue=" + rue + ", codePostal=" + codePostal + ", ville=" + ville + ", client=" + client + "]";
	}
	
	
	
}
