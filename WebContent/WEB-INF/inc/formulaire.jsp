<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>	
  <div class="form-group">
    <label for="nom" >Nom</label>		    
      <input type="text"  class="form-control" id="nom" name="nom" value="<c:out value='${ nomSaisi }' />">		   
  </div>
  <div class="form-group">
    <label for="prenom" >Prénom</label>		    
      <input type="text" class="form-control" name="prenom" id="prenom" value="<c:out value='${ prenomSaisi }' />">		    
  </div>
  <div class="form-group">
    <label for="phone" >Téléphone</label>		    
      <input type="text"  class="form-control" id="phone" name="phone" value="<c:out value='${ telephoneSaisi }' />" >
  </div>
   <div class="form-group">
    <label for="email" >Email</label>		    
      <input type="text"  class="form-control" id="email" name="email" value="<c:out value='${ emailSaisi }' />" >
   </div>			   
	  
</body>
</html>