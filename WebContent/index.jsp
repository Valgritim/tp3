<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css"/>
	<title>Menu de création</title>
</head>
<body style="background-color: blue;">
	<c:import url="/WEB-INF/inc/navBar.jsp"/>
	<div class="container text-center mt-5">
		
		<h1>Bienvenue dans le menu de création</h1>
		<h3>Choisissez la création dans la barre de navigation</h3>
	</div>

</body>
</html>