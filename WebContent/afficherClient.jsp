<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "org.eclipse.model.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="https://fonts.googleapis.com/css?family=Play&display=swap" rel="stylesheet"> 
  <link rel="stylesheet" href="css/style.css)"/>
<title>Fiche client</title>
</head>
<body>
	<c:import url="/WEB-INF/inc/navBar.jsp"/>
	<div class="container">
		<div class="alert alert-success mt-5"><c:out value="${message}"/></div>
		<div class="card" style="width:25rem">	  
		  <div class="card-body">
		    <h5 class="card-title">Fiche client</h5>			    
		  </div>
		  <ul class="list-group list-group-flush">
		    <li class="list-group-item">Prénom : <c:out value="${client.prenom}"/></li>	
		    <li class="list-group-item">Nom : <c:out value="${client.nom}"/></li>	  
		    <li class="list-group-item">Tel : <c:out value="${client.telephone}"/></li> 
		    <li class="list-group-item">Email : <c:out value="${client.email}"/></li>   
		    	    
		  </ul>
		</div>		
	</div>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>

</body>
</html>