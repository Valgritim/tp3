<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
      <link rel="stylesheet" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Play&display=swap" rel="stylesheet"> 
    <title>Formulaire d'ajout d'une adresse</title>
</head>
<body class="bg-gra-01">
	<c:import url="/WEB-INF/inc/navBar.jsp"/>
	<div class="container" style="margin-top:50px; width:50%;">
	
		<c:forEach items="${ messages }" var="message" varStatus="status">
			<div class='alert alert-danger mt-5'>
  			 	 	<c:out value="${ message }" /> 
  			</div>
		</c:forEach>	
		<div class="card p-3">
			<fieldset>
				<legend>Créer une adresse</legend>
					<form method="post" action="creationAdresse">
					  <c:import url="/WEB-INF/inc/formulaire.jsp"/>
					  <div class="form-group">
					    <label for="rue" >Rue</label>		    
					      <input type="text"  class="form-control" id="rue" name="rue" value="<c:out value='${ rueSaisie }' />" >		   
					  </div>
					  <div class="form-group">
					    <label for="cp" >Code postal</label>		    
					      <input type="text"  class="form-control" id="cp" name="codePostal" value=<c:out value='${ codePostalSaisi }' /> >		   
					  </div>
					  <div class="form-group">
					    <label for="ville" >Ville</label>		    
					      <input type="text"  class="form-control" id="ville" name="ville" value=<c:out value='${ villeSaisie }' />>		   
					  </div>
					  <input type="submit" class="btn btn-primary" value="Ajouter">
					</form>
			</fieldset>
		</div>
	</div>
	<div class="mb-5"></div>
	<script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
</body>
</html>